/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import es.bsc.inb.elixir.openebench.elixibilitas.mongo.ISO8601DateTimeConverter;
import es.bsc.inb.elixir.openebench.elixibilitas.mongo.PatchedJsonWriter;
import es.bsc.inb.elixir.openebench.elixibilitas.mongo.ToolsPrimaryKeys;
import es.bsc.inb.elixir.openebench.elixibilitas.rest.ToolsMonitoringEndpoint;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.bson.BsonDocument;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.bson.LazyBSONCallback;
import org.bson.LazyBSONDecoder;
import org.bson.RawBsonDocument;
import org.bson.codecs.BsonDocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;

/**
 * Serializes BSON values stream as JSON Array of values.
 * 
 * @author Dmitry Repchevsky
 */

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class BsonArrayBodyWriter
         implements MessageBodyWriter<StreamingOutput> {

    @Inject
    private URIInfoProducer uriInfoProducer;

    private JsonWriterSettings settings;
    private BsonDocumentCodec codec;

    private ExecutorService executor;
    
    @PostConstruct
    public void init() {
        settings = JsonWriterSettings.builder().
                dateTimeConverter(new ISO8601DateTimeConverter()).build(); 
        codec = new BsonDocumentCodec();
        executor = Executors.newCachedThreadPool();
    }

    @Override
    public boolean isWriteable(Class<?> type, Type generic, 
            Annotation[] annotations, MediaType mediaType) {
        return StreamingOutput.class.isAssignableFrom(type) && 
               MediaType.APPLICATION_JSON_TYPE.isCompatible(mediaType);
    }

    @Override
    public void writeTo(StreamingOutput sout, Class<?> clazz, Type generic, 
            Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, 
            Object> headers, OutputStream out)
            throws IOException, WebApplicationException {
        
        try (JsonWriter writer = new PatchedJsonWriter(new OutputStreamWriter(out), settings);
             PipedOutputStream o = new PipedOutputStream();
             PipedInputStream in = new PipedInputStream(o)) {


            executor.execute(() -> {
                try {
                    sout.write(o);
                } catch (IOException ex) {
                    Logger.getLogger(BsonArrayBodyWriter.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            
            writer.writeStartArray();
            
            final LazyBSONDecoder decoder = new LazyBSONDecoder();
            final LazyBSONCallback callback = new LazyBSONCallback() {
                @Override
                public void gotBinary(String name, byte type, byte[] data) {
                    final RawBsonDocument doc = new RawBsonDocument(data);
                    try {
                        final BsonDocument bson = doc.decode(codec);
                        final BsonDocument pk = bson.remove("_id").asDocument();
                        
                        final String _id = ToolsPrimaryKeys.getID(pk).getValue();
                        final String id = UriBuilder.fromUri(uriInfoProducer.getUriInfo().getBaseUri())
                                .path(ToolsMonitoringEndpoint.class)
                                .path(_id).build().toString();
                        bson.append("@id", new BsonString(id));
                        
                        bson.append("@type", pk.get("type", new BsonNull()));
                        bson.append("namespace", pk.get("nmsp", new BsonNull()));
                        bson.append("label", pk.get("id", new BsonString("")));
                        bson.append("softwareVersion", pk.get("ver", new BsonNull()));

                        codec.encode(writer, bson, EncoderContext.builder().build());
                        writer.flush();
                    } catch (WebApplicationException ex) {
                        Logger.getLogger(JSONLDStreamBodyWriter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
            
            try {
                while (in.available() >= 0) {
                    decoder.decode(in, callback);
                }
            } catch(EOFException ex) {}
            
            writer.writeEndArray();
            writer.flush(); // writer does not flush on close() !?
        } catch(Exception ex) {
            Logger.getLogger(JSONLDStreamBodyWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
