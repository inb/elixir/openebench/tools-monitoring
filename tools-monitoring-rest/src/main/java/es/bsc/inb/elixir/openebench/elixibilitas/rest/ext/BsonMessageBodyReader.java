/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import org.bson.BsonValue;
import org.bson.codecs.BsonValueCodec;
import org.bson.codecs.DecoderContext;
import org.bson.json.JsonReader;

/**
 * @author Dmitry Repchevsky
 */

@Provider
@Consumes(MediaType.APPLICATION_JSON)
public class BsonMessageBodyReader implements MessageBodyReader<BsonValue> {

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, 
            MediaType mediaType) {
        return BsonValue.class.isAssignableFrom(type);
    }

    @Override
    public BsonValue readFrom(Class<BsonValue> type, Type genericType, Annotation[] annotations, 
            MediaType mediaType, MultivaluedMap<String, String> headers, InputStream stream) throws IOException, WebApplicationException {

        final BsonValueCodec codec = new BsonValueCodec();
        try (JsonReader reader = new JsonReader(new InputStreamReader(stream))) {
            reader.readBsonType(); // decode() fails if not read !?
            return codec.decode(reader, DecoderContext.builder().build());
        }
    }
}
