/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

/**
 * @author Dmitry Repchevsky
 */

public class RDFPropertyFinder {
    
    public final Model model;
    
    public RDFPropertyFinder(Model model) {
        this.model = model;
    }
    
    public List<Statement> findStatements(Resource subject, String property) {
        final List<Statement> statements = new ArrayList();
        
        for (Statement statement : model.getStatements(subject, null, null)) {
            final IRI predicate = statement.getPredicate();

            final String namespace = predicate.getNamespace();
            if ((RDF.NAMESPACE.equals(namespace) ||
                RDFS.NAMESPACE.equals(namespace) ||
                OWL.NAMESPACE.equals(namespace)) &&
                !RDFS.LABEL.equals(predicate)) {
                    continue; // skip
            }
            if (property.equals(predicate.getLocalName())) {
                statements.add(statement);
            }
        }
        
        // look for parents
        for (Statement statement : model.getStatements(subject, RDFS.SUBCLASSOF, null)) {
            final Value obj = statement.getObject();
            if (obj.isResource()) {
                statements.addAll(findStatements((Resource)obj, property));
            }
        }
        return statements;
    }
}
