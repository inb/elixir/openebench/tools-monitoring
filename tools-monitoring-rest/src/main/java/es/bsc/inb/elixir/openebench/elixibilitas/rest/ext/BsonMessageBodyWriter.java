/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import es.bsc.inb.elixir.openebench.elixibilitas.mongo.ISO8601DateTimeConverter;
import es.bsc.inb.elixir.openebench.elixibilitas.mongo.PatchedJsonWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.annotation.PostConstruct;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.bson.BsonValue;
import org.bson.codecs.BsonValueCodec;
import org.bson.codecs.EncoderContext;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;

/**
 * @author Dmitry Repchevsky
 */

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class BsonMessageBodyWriter
        implements MessageBodyWriter<BsonValue> {

    private BsonValueCodec codec;
    private JsonWriterSettings settings;

    public BsonMessageBodyWriter() {
        codec = new BsonValueCodec();
        settings = JsonWriterSettings.builder().
                dateTimeConverter(new ISO8601DateTimeConverter()).build();
    }

    @Override
    public boolean isWriteable(Class<?> type, Type generic, 
            Annotation[] annotations, MediaType mediaType) {
        return BsonValue.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(BsonValue value, Class<?> type, Type generic, 
            Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> headers, 
            OutputStream out) throws IOException, WebApplicationException {

        try (JsonWriter writer = new PatchedJsonWriter(new OutputStreamWriter(out), settings)) {
            codec.encode(writer, value, EncoderContext.builder().build());
            writer.flush(); // writer does not flush on close() !?
        }
    }
}
