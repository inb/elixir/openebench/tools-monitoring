/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.mongo;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.ServletContext;

/**
 * MongoDB driver producer (allows MongoClient injection).
 * 
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class MongoDBProducer implements Serializable {

    public final static String MONGODB_URI_PROPERTY = "mongodb.url";

    @Inject 
    private ServletContext ctx;

    private MongoClient mc;
    private MongoDatabase db;
    
    @PostConstruct
    public void init() {
        String mongodb_url = System.getenv(MONGODB_URI_PROPERTY);
        if (mongodb_url == null || mongodb_url.isEmpty()) {
            mongodb_url = ctx.getInitParameter(MONGODB_URI_PROPERTY);
        }
        ConnectionString con = new ConnectionString(mongodb_url);

        mc = MongoClients.create(con);
        db = mc.getDatabase(con.getDatabase());
    }
    
    @PreDestroy
    public void destroy() {
        mc.close();
    }
    
    @Produces
    public MongoDatabase getMongoDatabase() {
        return db;
    }
}