/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonType;
import org.bson.BsonValue;
import org.bson.codecs.BsonValueCodec;
import org.bson.codecs.DecoderContext;
import org.bson.json.JsonReader;

/**
 * Deserializes JSON Array as a stream of BSON values.
 * 
 * @author Dmitry Repchevsky
 */

@Provider
@Consumes(MediaType.APPLICATION_JSON)
public class BsonArrayBodyReader implements MessageBodyReader<Stream<BsonValue>> {

    @Context
    private UriInfo uriInfo;

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, 
            MediaType mediaType) {
        return Stream.class.isAssignableFrom(type);
    }

    @Override
    public Stream<BsonValue> readFrom(Class<Stream<BsonValue>> type, 
            Type genericType, Annotation[] annotations, MediaType mediaType, 
            MultivaluedMap<String, String> headers, InputStream stream) 
            throws IOException, WebApplicationException {

        return StreamSupport.stream(
                Spliterators.spliterator(new BsonValueIterator(stream), 0, 0), false)
                .map(this::fixID);
    }
    
    /*
     * Convert provided by a user @id to the _id.
     * The @id may be either the a 'full' URL 
     * (e.g. 'https://openebench.bsc.es/bioschemas/tools/biotools:trimal:1.4/cmd')
     * or a short string form (e.g. 'biotools:trimal:1.4/cmd').
     * N.B. if the 'full' URL doesn't match the real server endpoint, 
     * the object will be ignored (no _id assigned).
     */
    private BsonValue fixID(BsonValue value) {
        if (value.isDocument()) {
            final BsonDocument doc = value.asDocument();
            final BsonValue id = doc.remove("@id");
            if (id != null && id.isString()) {
                final String path = uriInfo.getAbsolutePath().toString();
                final String _id = id.asString().getValue();
                try {
                    final URI uri = URI.create(_id);
                    if (uri.isAbsolute() && _id.startsWith(path)) {
                        doc.put("_id", new BsonString(_id.substring(path.length())));
                    } else {
                        doc.put("_id", new BsonString(_id));
                    }
                } catch (IllegalArgumentException ex) {
                    doc.remove("_id");
                }
            } else {
                doc.remove("_id");
            }
        }
        return value;
    }
    
    /**
     * Iterator over a stream that contains JSON Array.
     */
    public static class BsonValueIterator implements Iterator<BsonValue>,
            Closeable, AutoCloseable {

        private final BsonValueCodec codec;
        private final JsonReader reader;
        private BsonType type;

        public BsonValueIterator(InputStream stream) {
            codec = new BsonValueCodec();
            reader = new JsonReader(new InputStreamReader(stream));
            
            if (reader.readBsonType() == BsonType.ARRAY) {
                reader.readStartArray();
            }
        }

        @Override
        public boolean hasNext() {
            if (type == null) {
                type = reader.readBsonType();
            }
            return type != BsonType.END_OF_DOCUMENT;
        }

        @Override
        public BsonValue next() {
            if (hasNext()) {
                type = null;
                return codec.decode(reader, DecoderContext.builder().build());
            }
            return null;
        }
        
        @Override
        public void close() throws IOException {
            reader.close();
        }
    }
}
