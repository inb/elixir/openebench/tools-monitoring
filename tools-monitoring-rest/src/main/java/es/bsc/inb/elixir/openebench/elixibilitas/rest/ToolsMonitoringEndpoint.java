/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest;

import com.mongodb.client.MongoDatabase;
import es.bsc.inb.elixir.openebench.elixibilitas.mongo.ToolsRepository;
import es.bsc.inb.elixir.openebench.elixibilitas.rest.ext.ContentRange;
import es.bsc.inb.elixir.openebench.elixibilitas.rest.ext.MediaTypes;
import es.bsc.inb.elixir.openebench.elixibilitas.rest.ext.Range;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;

/**
 * @author Dmitry Repchevsky
 */

@Path("/tools/")
@ApplicationScoped
public class ToolsMonitoringEndpoint {
 
    @Resource
    private ManagedExecutorService executor;

    @Inject MongoDatabase db;
    
    private ToolsRepository repo;
    
    @PostConstruct
    public void init() {
        repo = new ToolsRepository(db);
    }

    @OPTIONS
    @Path("/")
    public Response getTools() {
         return Response.ok()
                 .header("Access-Control-Allow-Headers", "Range")
                 .header("Access-Control-Expose-Headers", "Accept-Ranges")
                 .header("Access-Control-Expose-Headers", "Content-Range")
                 .build();
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public void getAllTools(
            @HeaderParam("Range") Range range,
            @QueryParam("from") Long from,
            @QueryParam("to") Long to,
            @Suspended AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(
                    getAllToolsAsync(range == null ? from : range.getFirstPos(), 
                                     range == null ? to : range.getLastPos())
                            .header("Access-Control-Allow-Headers", "Range")
                            .header("Access-Control-Expose-Headers", "Accept-Ranges")
                            .header("Access-Control-Expose-Headers", "Content-Range")
                            .build());
        });    
    }

    @GET
    @Path("/")
    @Produces("application/ld+json")
    public void getJSONLDGraph(
            @HeaderParam("Range") Range range,
            @QueryParam("from") Long from,
            @QueryParam("to") Long to,
            @Suspended AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(
                    getAllToolsAsync(range == null ? from : range.getFirstPos(), 
                                     range == null ? to : range.getLastPos())
                            .header("Access-Control-Allow-Headers", "Range")
                            .header("Access-Control-Expose-Headers", "Accept-Ranges")
                            .header("Access-Control-Expose-Headers", "Content-Range")
                            .build());
        });    
    }

    @GET
    @Path("/{id : .*}")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public void get(@PathParam("id") String id,
                    @Context UriInfo uriInfo,
                    @Suspended AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getAsync(id, MediaType.APPLICATION_JSON, uriInfo).build());
        });    
    }
    
    @GET
    @Path("/{id : .*}")
    @Produces("application/ld+json")
    public void getJsonLd(@PathParam("id") String id,
                    @Context UriInfo uriInfo,
                    @Suspended AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getAsync(id, "application/ld+json", uriInfo).build());
        });    
    }
    
    @PUT
    @Path("/{id : .*}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("submitter")
    public void put(@PathParam("id") String id,
                    BsonValue bson,
                    @Context UriInfo uriInfo,
                    @Context SecurityContext ctx,
                    @Suspended AsyncResponse asyncResponse) {
        
        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            asyncResponse.resume(Response.status(Response.Status.NETWORK_AUTHENTICATION_REQUIRED).build());
            return;
        }

        if (!bson.isDocument()) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }
        
        executor.submit(() -> {
            asyncResponse.resume(putAsync(id, bson.asDocument(), uriInfo, principal).build());
        });
    }
    
    @POST
    @Path("/")
    @Consumes({MediaType.APPLICATION_JSON, "text/uri-list"})
    @RolesAllowed("submitter")
    public Response post(Stream<BsonDocument> stream,
                         @Context HttpHeaders headers,
                         @Context SecurityContext ctx) {
        
        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.NETWORK_AUTHENTICATION_REQUIRED).build();
        }

        final Stream<BsonDocument> out = repo.write(stream, principal);
        
        final List<MediaType> acceptible = headers.getAcceptableMediaTypes();
        
        if (acceptible.contains(MediaType.APPLICATION_JSON_TYPE) ||
            acceptible.contains(MediaTypes.TEXT_URI_LIST)) {
            return Response.accepted(out).build();
        }

        final StreamingOutput s = (OutputStream o) -> {
            // force to push all
            final Iterator<BsonDocument> iter = out.iterator();
            while (iter.hasNext()) {
                iter.next();
            }
        };
        return Response.accepted(s).build();
    }
    
    @GET
    @Path("/log/{id}/{type}{path:.*}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getToolsLog(@PathParam("id") String id,
                            @PathParam("type") String type,
                            @PathParam("path") String path,
                            @QueryParam("from") String from,
                            @QueryParam("to") String to,
                            @QueryParam("limit") Integer limit,
                            @Suspended AsyncResponse asyncResponse) {
        if (path == null || path.isEmpty()) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
        }
        executor.submit(() -> {
            asyncResponse.resume(getToolsLogAsync(id + "/" + type, path, from, to, limit).build());
        });
    }

    private ResponseBuilder getAsync(String _id, String mediaType, UriInfo uriInfo) {
        final BsonDocument doc = repo.get(_id);
        if (doc == null) {
            return Response.status(Response.Status.NOT_FOUND);
        }

        doc.remove("_id");
        
        final String id = UriBuilder.fromUri(uriInfo.getBaseUri())
                .path(ToolsMonitoringEndpoint.class)
                .path(_id).build().toString();
        doc.append("@id", new BsonString(id));
        
        return Response.ok(doc, mediaType);
    }
    
    private ResponseBuilder putAsync(String _id, BsonDocument bson, UriInfo uriInfo, Principal principal) {
        bson.append("_id", new BsonString(_id));
        
        final BsonDocument doc = repo.put(bson, principal);
        
        if (doc == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        
        doc.remove("_id");
        
        final String id = UriBuilder.fromUri(uriInfo.getBaseUri())
                .path(ToolsMonitoringEndpoint.class)
                .path(_id).build().toString();
        doc.append("@id", new BsonString(id));
        
        return Response.ok(doc, MediaType.APPLICATION_JSON);
    }
    
    private Response.ResponseBuilder getToolsLogAsync(String id, String field, String  from, String  to, Integer limit) {
        final BsonArray array = repo.findLog(id, field, from, to, limit);
        if (array == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        if (array.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND);
        }
        
        return Response.ok(array, MediaType.APPLICATION_JSON);
    }

    private ResponseBuilder getAllToolsAsync(Long from, Long to) {
        
        final Long limit = (from == null || to == null) ? to : to - from;

        StreamingOutput stream = (OutputStream out) -> {
            repo.stream(out, from, limit);
        };

        final long count = repo.count();
        
        final ContentRange range = new ContentRange("ComputationalTool", from, to, count);
        
        ResponseBuilder response = from == null && to == null 
                ? Response.ok() : Response.status(Response.Status.PARTIAL_CONTENT);
        
        return response.header("Accept-Ranges", "ComputationalTool")
                .header("Content-Range", range.toString())
                .entity(stream);
    }
}
