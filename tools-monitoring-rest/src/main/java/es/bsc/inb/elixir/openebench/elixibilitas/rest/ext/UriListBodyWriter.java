/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import es.bsc.inb.elixir.openebench.elixibilitas.rest.ToolsMonitoringEndpoint;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.stream.Stream;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.bson.BsonDocument;
import org.bson.BsonValue;

/**
 * @author Dmitry Repchevsky
 */

@Provider
@Produces("text/uri-list")
public class UriListBodyWriter
         implements MessageBodyWriter<Stream<BsonValue>> {

    @Context
    private UriInfo uriInfo;

    @Override
    public boolean isWriteable(Class<?> type, Type generic, 
            Annotation[] annotations, MediaType mediaType) {
        return Stream.class.isAssignableFrom(type) &&
               MediaTypes.TEXT_URI_LIST.isCompatible(mediaType);
    }

    @Override
    public void writeTo(Stream<BsonValue> stream, Class<?> clazz, Type generic, 
            Annotation[] annotations, MediaType mediaType, 
            MultivaluedMap<String, Object> headers, OutputStream out)
            throws IOException, WebApplicationException {
        
        final Iterator<BsonValue> iter = stream.iterator();
        try (Writer writer = new OutputStreamWriter(out, StandardCharsets.UTF_8)) {
            while (iter.hasNext()) {
                BsonValue value = iter.next();
                if (value != null) {
                    if (value.isDocument()) {
                        final BsonDocument doc = value.asDocument();
                        final String _id = doc.remove("_id").asString().getValue();
                        final String id = UriBuilder.fromUri(uriInfo.getBaseUri())
                                .path(ToolsMonitoringEndpoint.class)
                                .path(_id).build().toString();

                        writer.write(id);
                        writer.write('\n');
                        writer.flush();
                    }
                }
            }
        }
    }
}
