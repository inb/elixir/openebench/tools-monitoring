/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.ext;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

/**
 * @author Dmitry Repchevsky
 */

@Provider
@Produces("application/ld+json")
public class JSONLDMessageBodyWriter extends BsonMessageBodyWriter
        implements MessageBodyWriter<BsonValue> {

    @Inject 
    private ServletContext ctx;

    @Inject
    private URIInfoProducer uriInfoProducer;

    private RDFPropertyFinder inferer;
    
    @PostConstruct
    public void init() {
        try (InputStream bioschemas = ctx.getResourceAsStream("/bioschemas.jsonld")) {
            final Model model = Rio.parse(bioschemas, RDFFormat.JSONLD);
            inferer = new RDFPropertyFinder(model);
        } catch (IOException ex) {
            Logger.getLogger(JSONLDMessageBodyWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void writeTo(BsonValue value, Class<?> clazz, Type generic, 
            Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> headers, 
            OutputStream out) throws IOException, WebApplicationException {

        if (value.isDocument()) {
            // add json-ld context
            final BsonDocument context = new BsonDocument("@import", new BsonString(getOntologyURI()));
            value.asDocument().append("@context", context);
            extend(value.asDocument());
        }
        super.writeTo(value, clazz, generic, annotations, mediaType, headers, out);
    }
    
    public boolean extend(BsonDocument doc) {
        final IRI iri = SimpleValueFactory.getInstance().createIRI("https://schema.org/SoftwareApplication");
        
        // cast all types to the 'bioschemas:SoftwareApplication'
        doc.append("@type", new BsonString("SoftwareApplication"));
        return extend(doc, iri);
    }
    
    private boolean extend(BsonValue value, IRI iri) {
        switch(value.getBsonType()) {
            case ARRAY:
                boolean extended = true;
                for (BsonValue v : value.asArray().getValues()) {
                    extended &= extend(v, iri);
                }
                return extended; // reurn true only if all objects was extended
            case DOCUMENT:
                final BsonDocument doc = value.asDocument();
                final BsonValue type = doc.get("@type");
                if (type != null && type.isString() && 
                    !iri.getLocalName().equals(type.asString().getValue())) {
                    return false; // mismatch between provided and explicit @type
                }
                
                for (Entry<String, BsonValue> entry : List.copyOf(doc.entrySet())) {
                    // There can be several statement for the property e.g:
                    // schema:CreativeWork schema:author schema:Person
                    // schema:CreativeWork schema:author schema:Organization
                    // we take the first property namespace to simplify code.
                    Namespace namespace = null;

                    final String name = entry.getKey();
                    final List<Statement> statements = inferer.findStatements(iri, name);
                    for (Statement statement : statements) {
                        if (namespace == null) {
                            final IRI predicate = statement.getPredicate();
                            final String nmsp = predicate.getNamespace();
                            namespace = inferer.model.getNamespaces().stream()
                                    .filter(n -> n.getName().equals(nmsp)).findFirst().orElse(null);                            
                        }
                        final Value obj = statement.getObject();
                        if (obj.isIRI()) {
                            extend(entry.getValue(), (IRI)obj);
                        }
                    }
                    if (namespace != null) {
                        doc.append(namespace.getPrefix() + ":" + name, doc.remove(name));
                    }
                }
                doc.append("@type", new BsonString(iri.toString()));
                return true;
        }
        return false;
    }
 
    private String getOntologyURI() {
        return uriInfoProducer.getUriInfo()
                .getBaseUriBuilder()
                .path("/bioschemas.jsonld").build().toString();
    }
}
