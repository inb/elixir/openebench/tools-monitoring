/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.rest.application;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Dmitry Repchevsky
 */

@Path("/")
@ApplicationScoped
public class OntologyEndpoint {
    
    @Inject 
    private ServletContext ctx;

    @GET
    @Path("/bioschemas.jsonld")
    public Response getBioSchemasOntology() {
        return Response.ok(ctx.getResourceAsStream("/bioschemas.jsonld"), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/oebtools.jsonld")
    public Response getOEBOntology() {
        return Response.ok(ctx.getResourceAsStream("/oebtools.jsonld"), MediaType.APPLICATION_JSON).build();
    }
}
