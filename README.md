# tools-monitoring

Bioschemas format tools registry for openebench


| OpenEBench property          |       predicate      |       type      |              BioTools            |
|------------------------------|----------------------|-----------------|----------------------------------|
|                              |                      |                 |                                  |
|  schema:name                 | owl:DatatypeProperty | xsd:string      | "name"                           |
|  schema:description          | owl:DatatypeProperty | xsd:string      | "description"                    |
|  schema:url                  | owl:DatatypeProperty | xsd:anyURI      | "homepage"                       |
|  schema:softwareVersion      | owl:DatatypeProperty | xsd:string      | "version"                        |
|  schema:applicationCategory  | owl:DatatypeProperty | xsd:string      | "toolType"                       |
|  schema:operatingSystem      | owl:DatatypeProperty | xsd:string      | "operatingSystem"                |
|  schema:programmingLanguage  | owl:DatatypeProperty | xsd:string      | "language"                       |
|  schema:license              | owl:DatatypeProperty | xsd:string      | "license"                        |
|  schema:author               | owl:ObjectProperty   | schema:Person   | "credit[@typeRole='Developer']"  |
|  schema:maintainer           | owl:ObjectProperty   | schema:Person   | "credit[@typeRole='Maintainer']" |
|  schema:codeRepository       | owl:DatatypeProperty | xsd:anyURI      | "link[@type='Repository']/url"   |
|  schema:downloadUrl          | owl:DatatypeProperty | xsd:anyURI      | "download[@type='Binaries']/url" |
|  schema:keywords             | owl:DatatypeProperty | xsd:string      | "collectionID"                   |
