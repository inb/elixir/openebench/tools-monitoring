#!/usr/bin/env python3

import sys
import io
import aiohttp
import asyncio
import urllib.request
import concurrent.futures
import json
import yaml
import bz2
import tarfile

HTTP_GET_PARALLEL_TASKS = 2
WRITE_BUFFER_SIZE = 16384

BIOCONDA_REPOS = [
    "https://conda.anaconda.org/bioconda/linux-64/repodata.json.bz2",
    "https://conda.anaconda.org/bioconda/osx-64/repodata.json.bz2",
    "https://conda.anaconda.org/bioconda/noarch/repodata.json.bz2"
  ]

ANACONDA_TOOL_ENDPOINT = "https://anaconda.org/bioconda/{name}/{version}/download/{platform}/{file}"

TOOLS_MONITORING_API_URL = "https://openebench.bsc.es/bioschemas/tools/"

async def main():

  if (len(sys.argv) < 2):
    sys.exit("no parameters! either credentials (e.g. 'username:password') or file name is expected.")  
  
  if (':' in sys.argv[1]): 
    await post_tools(sys.argv[1])
  else:
    await write_tools(sys.argv[1])

################################################################################
# write all bioconda tools to the file
################################################################################
async def write_tools(fname):

  packages = await get_bioconda_tools()


  reader = BiocondaReader(packages)
  try:
    with open(fname, "wb") as f:
      while(True):
        result = await asyncio.gather(asyncio.to_thread(reader.read, WRITE_BUFFER_SIZE))
        chunk = result.pop()
        if (not chunk):
          break
        f.write(chunk)
  finally:
    reader.close()

################################################################################
# push all bioconda tools to the server
################################################################################
async def post_tools(credentials):

  packages = await get_bioconda_tools()

  tokens = credentials.split(':')

  user = tokens[0]
  password = tokens[1]

  auth = aiohttp.BasicAuth(user, password)

  headers = {'User-agent': 'python', 'Accept': 'text/uri-list', 'Content-type': 'application/json; charset=UTF-8', 'Content-Encoding': 'gzip'}

  try:
    async with aiohttp.ClientSession() as ses:
      async with ses.post(TOOLS_MONITORING_API_URL, data=BiocondaReader(packages), headers=headers, auth=auth, timeout=36000) as resp:
        async for line in resp.content:
          print(line.decode(), end="")
  except:
    pass

async def get_bioconda_tools():

  async with aiohttp.ClientSession() as ses:
    repodatas = await load_repodatas(ses)
    return aggregate_packages(repodatas)

async def load_repodatas(ses):
 
  tasks = []
  for url in BIOCONDA_REPOS:
    task = asyncio.create_task(load_repodata(ses, url))
    tasks.append(task)

  return await asyncio.gather(*tasks)

async def load_repodata(ses, url):

  headers = {'User-agent': 'python'}

  async with ses.get(url, headers=headers) as response:
    if response.status >= 300:
      response.raise_for_status()
    payload = await response.read()
    return json.loads(bz2.decompress(payload))
 

def aggregate_packages(repodatas):

  tools = {}
  for repodata in repodatas:
    packages = repodata.get("packages")
    for key, val in list(packages.items()):
      name = val.get("name")
      version = val.get("version")
      build = val.get("build_number")

      stored = tools.get(name + ':' + version)
      if (stored == None or stored[0] <= build):
        tools[name + ':' + version] = (build, key, val)

  return dict((key, val) for build, key, val in tools.values())

class BiocondaReader(io.RawIOBase):

    def __init__(self, packages):
        self.isclosed = None
        self.leftover = [ord('[')]

        self.loop = asyncio.new_event_loop()
        self.loop.set_default_executor(concurrent.futures.ThreadPoolExecutor(max_workers=HTTP_GET_PARALLEL_TASKS))
        self.iterator = BiocondaRepositoryIterator(packages)

    def readinto(self, buffer: bytearray):
        size = len(self.leftover) if (self.isclosed or len(self.leftover) > 0) else min(WRITE_BUFFER_SIZE, len(buffer))
        while len(self.leftover) < size:
            try:
                bioschemas = self.loop.run_until_complete(anext(self.iterator))
                if (self.isclosed != None):
                    self.leftover.append(ord(','))
                else:
                    self.isclosed = False
                self.leftover.extend(json.dumps(bioschemas).encode("utf-8"))
            except StopAsyncIteration:
                self.leftover.append(ord(']'))
                self.isclosed = True
                break

        if len(self.leftover) == 0:
            return 0

        output, self.leftover = self.leftover[:size], self.leftover[size:]
        buffer[:len(output)] = output
        return len(output)

    def close(self):
      super().close()
      self.loop.close()

    def readable(self):
        return True

class BiocondaRepositoryIterator:

  def __init__(self, packages):
    self.tasks = []
    self.iterator = iter(packages.items())
  def __aiter__(self):
    return self

  async def __anext__(self):

    while len(self.tasks) < HTTP_GET_PARALLEL_TASKS:
      try:
        item = next(self.iterator)
        coro = asyncio.to_thread(self.get_tool_meta, item)
        self.tasks.append(asyncio.create_task(coro))
      except StopIteration:
        break

    if len(self.tasks) == 0:
      raise StopAsyncIteration
    
    finished, pending = await asyncio.wait(self.tasks, return_when=asyncio.FIRST_COMPLETED)
    first = finished.pop()
    self.tasks.remove(first)

    return first.result()
        
  def get_tool_meta(self, item):

    fname = item[0]
    name = item[1].get("name")
    version = item[1].get("version")
    subdir = item[1].get("subdir")

    url = ANACONDA_TOOL_ENDPOINT.format(name=name, version=version, platform=subdir, file=fname)

    # bioconda prefixes tools for R or Bioconductor
    if (name.startswith('bioconductor-')):
      name = name[13:]
    elif(name.startswith('r-')):
      name = name[2:]

    bioschemas = {'name': name, 'version': version, 'downloadUrl': [url], 'applicationCategory': 'Library'}

    req = urllib.request.Request(url)
    req.add_header('User-agent', 'python')
    try:
      with urllib.request.urlopen(req) as res:
        with bz2.open(res, "rb") as stream:
          reader = Bzip2Reader(io.BufferedReader(stream))
          tar = tarfile.open(fileobj=reader, mode='r:')
          self.parse_tar(bioschemas, tar)
        
    except Exception as e:
      print('error: ', e, url)

    tool_type = "cmd" if (bioschemas["applicationCategory"] == "Command-line tool") else "lib"

    id = TOOLS_MONITORING_API_URL + 'bioconda:' + name.lower()
    if (version != None):
      id = id + ':' + version.split(" ", 1)[0] + '/' + tool_type
    else:
      id = id + '/' + tool_type

    bioschemas['@id'] = id
    return bioschemas

  def parse_tar(self, bioschemas, tar):
    for member in tar:
      if (member.isfile()):
        if ("info/index.json" == member.name):
          reader = tar.extractfile(member)
          meta = json.loads(reader.read())

          license = meta.get("license")
          if (license != None):
            bioschemas["license"] = [license]

          platform = meta.get("platform")
          if (platform != None):
            bioschemas["operatingSystem"] = [platform]

        if ("info/about.json" == member.name):
          reader = tar.extractfile(member)
          meta = json.loads(reader.read())
          home = meta.get("home")
          if (home != None):
            bioschemas["url"] = home

          summary = meta.get("summary")
          if (summary != None):
            bioschemas["description"] = summary

        if ("info/recipe/meta.yaml" == member.name):
          reader = tar.extractfile(member)
          meta = yaml.safe_load(reader)

          test = meta.get("test")
          if (isinstance(test, dict)):
            commands = test.get("commands")
            if (isinstance(commands, list) and len(commands) > 0):
              bioschemas["applicationCategory"] = "Command-line tool"

          about = meta.get("about")
          if (isinstance(about, dict)):
            home = about.get("home")
            if (home != None):
              bioschemas["url"] = home

            license = about.get("license")
            if (license != None):
              bioschemas["license"] = [license]

            summary = about.get("summary")
            if (summary != None):
              bioschemas["description"] = summary

          source = meta.get("source")
          if (isinstance(source, dict)):
            git = source.get('git_url')
            if (isinstance(git, list)):
              bioschemas["codeRepository"] = git
            elif(isinstance(git, str)):
              bioschemas["codeRepository"] = [git]

            url = source.get("url")
            if (isinstance(url, list)):
              bioschemas["sourcecodeUrl"] = url
            elif(isinstance(url, str)):
              bioschemas["sourcecodeUrl"] = [url]

          extra = meta.get("extra")
          if (isinstance(extra, dict)):
            identifiers = extra.get('identifiers')
            if(identifiers != None):
              if (isinstance(identifiers, str)):
                identifiers = [identifiers]

              for identifier in identifiers:
                if (identifier.startswith('doi:') or identifier.startswith('pubmed:')):
                  citations = bioschemas.get('citation')
                  if (citations == None):
                    bioschemas['citation'] = [identifier]
                  else:
                    citations.append(identifier)
                else:
                  ids = bioschemas.get('identifier')
                  if (ids == None):
                    bioschemas['identifier'] = [identifier]
                  else:
                    ids.append(identifier)
                
          return

class Bzip2Reader(io.RawIOBase):

    def __init__(self, stream):
        self.stream = stream
        self.pos = 0

    def read(self, size=-1):
        arr = self.stream.read(size)
        self.pos = self.pos + len(arr)
        return arr

    def readable(self):
        return True

    def seekable(self):
        return True

    def seek(self, offset, whence=0):
        if (whence == 2):
            return self.pos + len(self.stream.read(None))

        if (whence == 1):
            offset = self.pos + offset

        skipped = None           
        while (self.pos < offset and skipped != 0):
          skipped = len(self.stream.read(offset - self.pos))
          self.pos = self.pos + skipped

        return self.pos

    def tell(self):
        return self.pos

if __name__ == "__main__":
    asyncio.run(main())
