#!/usr/bin/env python3

import sys
import json
import io
import aiohttp
import asyncio
import urllib.request

BIOTOOLS_API_URL = "https://bio.tools/api/tool/"
TOOLS_MONITORING_API_URL = "http://localhost:8080/bioschemas/tools/"

async def main():

  if (len(sys.argv) < 2):
    sys.exit("no credentials! ('username:password')")  
    
  await post_tools(sys.argv[1])

async def post_tools(credentials):

  tokens = credentials.split(':')

  user = tokens[0]
  password = tokens[1]

  auth = aiohttp.BasicAuth(user, password)

  biotools = BiotoolsReader()

  headers = {'User-agent': 'python', 'Accept': 'text/uri-list', 'Content-type': 'application/json; charset=UTF-8', 'Content-Encoding': 'gzip'}

  async with aiohttp.ClientSession() as ses:
    async with ses.post(TOOLS_MONITORING_API_URL, data=biotools, headers=headers, auth=auth, timeout=3600) as resp:
      async for line in resp.content:
        print(line.decode(), end="")

class BiotoolsReader(io.RawIOBase):

    def __init__(self):
        self.iterator = BiotoolsIterator()
        self.leftover = []

    def readinto(self, buffer: bytearray):
        size = len(buffer)
        while len(self.leftover) < size:
            try:
                self.leftover.extend(next(self.iterator))
            except StopIteration:
                break

        if len(self.leftover) == 0:
            return 0

        output, self.leftover = self.leftover[:size], self.leftover[size:]
        buffer[:len(output)] = output
        return len(output)

    def readable(self):
        return True

class BiotoolsIterator:

  def __init__(self):
    self.tools = None
    self.iterator = None
    self.next_page = "["

  def __iter__(self):
    return self

  def __next__(self):
    while True:
      if (self.tools == None):
        if (self.iterator == None):
          if (self.next_page == "["):
            self.next_page = ""
            return "[".encode()     
          elif (self.next_page == "]"):
            raise StopIteration
          elif (self.next_page == None):
            self.next_page = "]" # closing json array ( ']' )
            return "]".encode()

          page = self.get_page()
          biotools = page.get('list')
          self.iterator = iter(biotools)
          self.next_page = page.get('next')

        try:
          biotool = next(self.iterator)
        except StopIteration:
          self.iterator = None
          continue

        bioschemas = BioschemasConverter.convert_tool(biotool)
        self.tools = iter(bioschemas)

      try:
        tool = next(self.tools)
        # print(tool.get("@id"))
        return json.dumps(tool).encode("utf-8")
      except StopIteration:
        self.tools = None

  def get_page(self):

    req = urllib.request.Request(BIOTOOLS_API_URL + self.next_page)
    req.add_header('Accept', 'application/json')

    res = urllib.request.urlopen(req)

    if(res.getcode() < 300):
      data = res.read()
      return json.loads(data)

    print("error reading data", req) 


class BioschemasConverter:

  def convert_tool(biotool):
    xid = biotool.get('biotoolsID')
    name = biotool.get('name')

    url = biotool.get('homepage')
    description = biotool.get('description')

    toolTypes = biotool.get('toolType')
    if (toolTypes == None or not toolTypes):
      toolTypes = ['ComputationalTool']

    versions = biotool.get('version')
    if (versions == None or not versions):
      versions = [None]

    tools = [{'@id': xid.lower(), 'name': name, 'url': url, 'description': description}]

    for version in versions:
      for toolType in toolTypes:
        id = TOOLS_MONITORING_API_URL + 'biotools:' + xid.lower()

        if (toolType != 'ComputationalTool'):
          toolType = BioschemasConverter.get_tool_type(toolType);

        if (toolType != 'ComputationalTool'):
          if (version != None):
            id = id + ':' + version.split(" ", 1)[0] + '/' + toolType
          else:
            id = id + '/' + toolType

        tool = {'@id': id, 'name': name, 'url': url, 'description': description, 'applicationCategory': toolType}

        BioschemasConverter.set_tags(biotool, tool)
        BioschemasConverter.set_os(biotool, tool)
        BioschemasConverter.set_programming_languages(biotool, tool)
        BioschemasConverter.set_licenses(biotool, tool)
        BioschemasConverter.process_credits(biotool, tool)
        BioschemasConverter.process_links(biotool, tool)
        BioschemasConverter.process_downloads(biotool, tool)
        BioschemasConverter.process_documentation(biotool, tool)
        BioschemasConverter.process_semantics(biotool, tool)

        tools.append(tool)

    return tools

  #
  # Add BioTools 'collectionID' as Bioschemas SoftwareApplication 'keywords' property.
  #

  def set_tags(biotool, tool):
    keywords = biotool.get('collectionID')
    if (keywords != None):
      tool['keywords'] = keywords

  #
  # Add BioTools 'operatingSystem' as Bioschemas SoftwareApplication 'operatingSystem' property.
  #

  def set_os(biotool, tool):
    os = biotool.get('operatingSystem')
    if (os != None):
      tool['operatingSystem'] = os

  #
  # Add BioTools 'language' as Bioschemas SoftwareApplication 'programmingLanguage' property.
  #
   
  def set_programming_languages(biotool, tool):
    languages = biotool.get('language')
    if (languages != None):
      if (not isinstance(languages, list)):
        languages = [languages]
      tool['programmingLanguage'] = languages

  #
  # Add BioTools 'license' as Bioschemas SoftwareApplication 'license' property.
  #

  def set_licenses(biotool, tool):
    licenses = biotool.get('license')
    if (licenses != None):
      if (not isinstance(licenses, list)):
        licenses = [licenses]
      for license in licenses:
        creative = {'name': license}
        lic = tool.get('license')
        if (lic == None):
          tool['license'] = [creative]
        else:
          lic.append(creative)

  #
  # Process BioTools 'credits' to setup various Bioschemas SoftwareApplication properties.
  #

  def process_credits(biotool, tool):
    credits = biotool.get('credit')
    if (credits != None):
      for credit in credits:
        BioschemasConverter.process_credit(credit, tool)

  #
  # Process BioTools 'credit' to setup various Bioschemas SoftwareApplication properties
  # ('schema:author', 'schema:maintainer', etc.).
  #

  def process_credit(credit, tool):

    contributor = {}  

    typeEntity = credit.get('typeEntity')
    if (typeEntity == 'Person'):
      contributor['@type'] = 'Person'
    elif(typeEntity == 'Project'):
      contributor['@type'] = 'Project'
    elif(typeEntity == 'Division'):
      contributor['@type'] = 'Organization'
    elif(typeEntity == 'Institute'):
      contributor['@type'] = 'Organization'
    elif(typeEntity == 'Consortium'):
      contributor['@type'] = 'Consortium'
    elif(typeEntity == 'Funding agency'):
      contributor['@type'] = 'FundingScheme'
 
    name = credit.get('name')
    if (name != None):
      contributor['name'] = name

    email = credit.get('email')
    if (email != None):
      contributor['email'] = email

    url = credit.get('url')
    if (url != None):
      contributor['url'] = url

    orcid = credit.get('orcidId')
    if (orcid != None):
      contributor['orcid'] = orcid

    comment = credit.get('comment')
    if (comment != None):
      contributor['comment'] = comment

    roles = credit.get('typeRole')
    if (roles != None):
      if (not isinstance(roles, list)):
        roles = [roles]
      for role in roles:    
        if (role == 'Developer'):
          authors = tool.get('author')
          if (authors == None):
            tool['author'] = [contributor]
          else:
            authors.append(contributor)
        elif (role == 'Maintainer'):
          maintainers = tool.get('maintainer')
          if (maintainers == None):
            tool['maintainer'] = [contributor]
          else:
            maintainers.append(contributor)

  #
  # Process BioTools 'documentation' to setup various Bioschemas SoftwareApplication properties.
  #

  def process_documentation(biotool, tool):
    documentation = biotool.get('documentation')
    if (documentation != None):
      for doc in documentation:
        url = doc.get('url')
        types  = doc.get('type')      
        if (url != None and types != None):
          if (not isinstance(types, list)):
            types = [types]
          for typ in types:        
            note = doc.get('note')
            if (typ == 'General'):
              creative = {'url': url}
              if (note != None):
                creative['description'] = note
              softhelp = tool.get('softwareHelp')
              if (softhelp == None):
                tool['softwareHelp'] = [creative]
              else:
                softhelp.append(creative)
          
  #
  # Process BioTools 'link' to setup various Bioschemas SoftwareApplication properties.
  #

  def process_links(biotool, tool):
    links = biotool.get('link')
    if (links != None and isinstance(links, list)):
      for link in links:
        url = link.get('url')
        types  = link.get('type')
        if (url != None and types != None):
          if (not isinstance(types, list)):
            types = [types]
          for typ in types:        
            if (typ == 'Repository'):
              repositories = tool.get('codeRepository')
              if (repositories == None):
                tool['codeRepository'] = [url]
              else:
                repositories.append(url)
     

  #
  # Process BioTools 'download' to setup various Bioschemas SoftwareApplication properties.
  #

  def process_downloads(biotool, tool):
    downloads = biotool.get('download')
    if (downloads != None and isinstance(downloads, list)):
      for download in downloads:
        url = download.get('url')
        if (url != None):
          typ  = download.get('type')
          if (typ == 'Binaries'):
            urls = tool.get('downloadUrl')
            if (urls == None):
              tool['downloadUrl'] = [url]
            else:
              urls.append(url)


  #
  # Process BioTools EDAM annotatins to setup various Bioschemas SoftwareApplication properties.
  #

  def process_semantics(biotool, tool):
    BioschemasConverter.set_topics(biotool, tool)
    functions = biotool.get('function')
    if (functions != None and isinstance(functions, list)):
      for function in functions:
        BioschemasConverter.set_operations(function, tool)
        BioschemasConverter.add_inputs(function.get('input'), tool)
        BioschemasConverter.add_outputs(function.get('output'), tool)


  #
  # Add BioTools 'topic.uri' as Bioschemas SoftwareApplication 'applicationSubCategory' properties.
  #
  
  def set_topics(biotool, tool):
    topics = biotool.get('topic')
    if (topics != None and isinstance(topics, list)):
      for topic in topics:
        uri = topic.get('uri')
        if (uri != None):
          subcategories = tool.get('applicationSubCategory');
        if (subcategories == None):
          tool['applicationSubCategory'] = [uri]
        else:
          subcategories.append(uri)

  def set_operations(function, tool):
    operations = function.get('operation')
    if (operations != None and isinstance(operations, list)):
      for operation in operations:
        uri = operation.get('uri')
        if (uri != None):
          features = tool.get('featureList');
        if (features == None):
          tool['featureList'] = [uri]
        else:
          features.append(uri)
        
  def add_inputs(datatypes, tool):
    if (datatypes != None and isinstance(datatypes, list)):
      for datatype in datatypes:
        param = BioschemasConverter.create_parameter(datatype)
        inputs = tool.get('input');
        if (inputs == None):
          tool['input'] = [param]
        else:
          inputs.append(param)

  def add_outputs(datatypes, tool):
    if (datatypes != None and isinstance(datatypes, list)):
      for datatype in datatypes:
        param = BioschemasConverter.create_parameter(datatype)
        outputs = tool.get('output');
        if (outputs == None):
          tool['output'] = [param]
        else:
          outputs.append(param)

  def create_parameter(datatype):
    param = {}
    data = datatype.get('data')
    if (data != None):
      uri = data.get('uri')
      if (uri != None):
        param['additionalType'] = uri
      term = data.get('term')
      if (term != None):
        param['name'] = term
    formats = datatype.get('format')
    if (formats != None and isinstance(formats, list)):
      for format in formats:
        uri = format.get('uri')
        if (uri != None):
          encoding = param.get('encodingFormat');
          if (encoding == None):
            param['encodingFormat'] = [uri]
          else:
            encoding.append(uri)
    return param

  #
  # Convert biotools 'toolType' into bioschemas types (subtypes of 'ComputationalTool')
  # @seeAlso https://biotools.readthedocs.io/en/latest/curators_guide.html#tool-type
  # @seeAlso https://github.com/bio-tools/biotoolsSchema/blob/bd559c053f0d0d29924b6dc21cfd42fd6c251adf/stable/biotools.xsd#L296
  #
  def get_tool_type(toolType):

    if (toolType == 'Bioinformatics portal'):
      return 'portal'
    if (toolType == 'Command-line tool'):
      return 'cmd'
    if (toolType == 'Database portal'):
      return 'db'
    if (toolType == 'Desktop application'):
      return 'app'
    if (toolType == 'Library'):
      return 'lib'
    if (toolType == 'Ontology'):
      return 'ontology'
    if (toolType == 'Plug-in'):
      return 'plugin'
    if (toolType == 'Script'):
      return 'script'
    if (toolType == 'SPARQL endpoint'):
      return 'sparql'
    if (toolType == 'Suite'):
      return 'suite'
    if (toolType == 'Web application'):
      return 'web'
    if (toolType == 'Web API'):
      return 'rest'
    if (toolType == 'Web service'):
      return 'soap'
    if (toolType == 'Workbench'):
      return 'workbench'
    if (toolType == 'Workflow'):
      return 'workflow'

    print('====================> ', toolType)

    return 'ComputationalTool'

if __name__ == "__main__":
    asyncio.run(main())
