/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas;

import es.bsc.inb.elixir.openebench.elixibilitas.mongo.BsonPatch;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class BsonPatchTest {
    
    public final static String JSON_1_A = "{\"name\": \"wiskey\"}";
    public final static String JSON_1_B = "{\"name\": \"wiskey\", \"type\": \"drink\"}";
    public final static String JSON_1_C = "[{\"op\": \"add\", \"path\": \"/type\", \"value\": \"drink\"}]";

    public final static String JSON_2_A = "{\"name\": \"wiskey\", \"type\": \"drink\"}";
    public final static String JSON_2_B = "{\"name\": \"wiskey\"}";
    public final static String JSON_2_C = "[{\"op\": \"remove\", \"path\": \"/type\"}]";

    public final static String JSON_3_A = "{\"name\": \"wiskey\"}";
    public final static String JSON_3_B = "{\"name\": \"brendy\"}";
    public final static String JSON_3_C = "[{\"op\": \"replace\", \"path\": \"/name\", \"value\": \"brendy\"}]";
    
    @Test
    public void test_1() {
        final BsonDocument bson_1_a = BsonDocument.parse(JSON_1_A);
        final BsonDocument bson_1_b = BsonDocument.parse(JSON_1_B);
        final BsonArray bson_1_c = BsonArray.parse(JSON_1_C);
        
        final BsonArray patch = BsonPatch.createDiff(bson_1_a, bson_1_b);
        
        Assert.assertEquals(bson_1_c, patch);
    }
    
    @Test
    public void test_2() {
        final BsonDocument bson_2_a = BsonDocument.parse(JSON_2_A);
        final BsonDocument bson_2_b = BsonDocument.parse(JSON_2_B);
        final BsonArray bson_2_c = BsonArray.parse(JSON_2_C);
        
        final BsonArray patch = BsonPatch.createDiff(bson_2_a, bson_2_b);
        Assert.assertEquals(bson_2_c, patch);
    }

    @Test
    public void test_3() {
        final BsonDocument bson_3_a = BsonDocument.parse(JSON_3_A);
        final BsonDocument bson_3_b = BsonDocument.parse(JSON_3_B);
        final BsonArray bson_3_c = BsonArray.parse(JSON_3_C);
        
        final BsonArray patch = BsonPatch.createDiff(bson_3_a, bson_3_b);
        Assert.assertEquals(bson_3_c, patch);
    }
}
