/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoWriteException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import java.security.Principal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.BsonArray;
import org.bson.BsonDateTime;
import org.bson.BsonDocument;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.conversions.Bson;

/**
 * The repository logger that stores document updates events in a form of 
 * JSON Patch operations.
 * 
 * @author Dmitry Repchevsky
 */

public class RepositotyLogger {
    
    public final static String LOG_SUFFIX = ".log";
    
    private final MongoCollection<BsonDocument> col;
            
    public RepositotyLogger(MongoCollection<BsonDocument> col) {
        this.col = col;
    }
    
    /**
     * Logs changes to the document.
     * 
     * @param principal the user principal that performed document changes
     * @param old the updated document (may be empty)
     * @param tgt the saved document
     * 
     * @return stored log document 
     */
    public BsonDocument log(Principal principal, BsonDocument old, BsonDocument tgt) {

        if (old == null) {
            old = new BsonDocument();
        }

        // add the '_id' and 'dateModified' to exclude them from the patch
        old.append("_id", tgt.get("_id"));
        old.append("dateModified", tgt.get("dateModified"));

        final BsonArray patch = BsonPatch.createDiff(old, tgt);
        final BsonDocument id = new BsonDocument("id", tgt.get("_id"));
        final BsonDocument bson = new BsonDocument()
                    .append("_id", id)
                    .append(ToolsMonitoringMeta.PROVENANCE_PROPERTY, new BsonString(principal.getName()))
                    .append("patch", patch);
        
        long timestamp = ZonedDateTime.now(ZoneId.of("Z")).toInstant().toEpochMilli();
        do {
            try {
                id.put("date", new BsonDateTime(timestamp++));
                col.insertOne(bson);
            } catch(MongoWriteException ex) {
                if (ex.getCode() == 11000) {
                    continue;
                }
                Logger.getLogger(RepositotyLogger.class.getName()).log(Level.SEVERE, null, ex);
            } catch(Exception ex) {
                Logger.getLogger(RepositotyLogger.class.getName()).log(Level.SEVERE, null, ex);
            }
            break;
        }
        while(true);

        return bson;
    }
    
    public BsonArray findLog(String id, String jpointer, String from, String to, Integer limit) {

        final BsonArray array = new BsonArray();

        try {
            Bson query = new BasicDBObject("_id.id", id);
            if (from != null && to == null) {
                query = Filters.and(query, Filters.gte("_id.date", from));
            } else if (from == null && to != null) {
                query = Filters.and(query, Filters.lte("_id.date", to));
            } else if (from != null && to != null) {
                query = Filters.and(query, Filters.gte("_id.date", from), Filters.lte("_id.date", to));
            }

            final ArrayList aggregation = new ArrayList();
            
            aggregation.add(Aggregates.match(query));
            aggregation.add(Aggregates.unwind("$patch"));
            aggregation.add(Aggregates.match(new BasicDBObject("patch.path", jpointer)));
            if (limit != null) {
                aggregation.add(Aggregates.sort(new BasicDBObject("_id.date", -1)));
                aggregation.add(Aggregates.limit(limit));
            }
            
            aggregation.add(Aggregates.sort(new BasicDBObject("_id.date", 1)));
            
            aggregation.add(Aggregates.project(new BasicDBObject("_id.date", 1).append("patch.value", 1)));
            
            AggregateIterable<BsonDocument> iterator = col.aggregate(aggregation).allowDiskUse(true);
            try (MongoCursor<BsonDocument> cursor = iterator.iterator()) {
                while (cursor.hasNext()) {
                    final BsonDocument doc = cursor.next();
                    final BsonDocument patch = doc.getDocument("patch");
                    if (patch != null) {
                        final BsonDocument record = new BsonDocument("date", 
                                doc.getDocument("_id").getDateTime("date"));
                        final BsonValue value = patch.get("value");
                        record.append("value", 
                                value == null ? BsonNull.VALUE : value);
                        array.add(record);
                    }
                }
            }
        } catch(Exception ex) {
            Logger.getLogger(RepositotyLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array;
    }

}
