/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;

/**
 * @author Dmitry Repchevsky
 */

public class ToolsPrimaryKeys {
    
    public static BsonDocument getPrimaryKey(BsonString _id) {
        final BsonDocument document;
        
        final String[] nodes = _id.getValue().split("/");
        
        final String[] id = nodes[0].split(":");        
        if (id.length > 2) {
            document = new BsonDocument()
                    .append("id", new BsonString(id[1]))
                    .append("nmsp", new BsonString(id[0]))
                    .append("ver", new BsonString((id[2])));
        } else if (id.length > 1) {
            document = new BsonDocument()
                    .append("id", new BsonString(id[1]))
                    .append("nmsp", new BsonString(id[0]));
        } else {
            document = new BsonDocument("id", new BsonString(id[0]));
        }

        if (nodes.length > 1) {
            document.append("type", new BsonString(nodes[1]));
        }

        return document;
    }

    public static BsonString getID(BsonDocument pk) {

        StringBuilder sb = new StringBuilder();
        
        final BsonValue nmsp = pk.get("nmsp");
        if (nmsp != null && nmsp.isString()) {
            sb.append(nmsp.asString().getValue()).append(':');
        }
        
        final BsonValue id = pk.get("id");
        if (id != null && id.isString()) {
            sb.append(id.asString().getValue());
        }

        
        final BsonValue version = pk.get("ver");
        if (version != null && version.isString()) {
            sb.append(':').append(version.asString().getValue());
        }
        
        final BsonValue type = pk.get("type");
        if (type != null && type.isString()) {
            sb.append('/').append(type.asString().getValue());
        }
        
        return new BsonString(sb.toString());
    }

}
