/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.bson.json.Converter;
import org.bson.json.StrictJsonWriter;

/**
 * Default MongoDB converter encodes DateTime 
 * into "datetime": {"$date": "YYYY-MM-DDTHH:MM:SS"}
 * 
 * @author Dmitry Repchevsky
 */

public class ISO8601DateTimeConverter implements Converter<Long> {

    @Override
    public void convert(Long value, StrictJsonWriter writer) {
        writer.writeString(ZonedDateTime
                .ofInstant(Instant.ofEpochMilli(value), ZoneId.of("Z"))
                .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }
}
