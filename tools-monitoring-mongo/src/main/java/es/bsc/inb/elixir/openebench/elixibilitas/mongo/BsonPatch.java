/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import java.util.Map;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonType;
import static org.bson.BsonType.ARRAY;
import static org.bson.BsonType.DOCUMENT;
import org.bson.BsonValue;

/**
 * @author Dmitry Repchevsky
 */

public final class BsonPatch {
    
    private BsonPatch() {}

    public static BsonArray createDiff(BsonDocument src, BsonDocument tgt) {
        return collectDiff(src, tgt, new BsonArray(), "", false);
    }
    
    /**
     * 
     * @param src the source BSON element to be compared with target (<code>tgt</code>). not null.
     * @param tgt the target BSON element compared with the source (<code>src</code>). may be null.
     * @param patch BSON array that collects Json Patch operations
     * @param pointer JSON pointer of the <code>src</code> element.
     * @param add the 'add' mode where <code>src</code> is to be add and <code>tgt<code> must be null.
     * 
     * @return updated <code>diff</code> patch object
     */
    private static BsonArray collectDiff(BsonValue src, BsonValue tgt, BsonArray patch, String pointer, boolean add) {
        final BsonType type = src.getBsonType();
        switch(type) {
            case DOCUMENT:
                if (tgt != null && tgt.isDocument()) {
                    collectDiff(src.asDocument(), tgt.asDocument(), patch, pointer, false);
                } else if (tgt != null) {
                    collectDiff(src.asDocument(), null, patch, pointer, false);
                    collectDiff(tgt.asDocument(), null, patch, pointer, true);
                } else if (add) {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("add"))
                        .append("path", new BsonString(pointer))
                        .append("value", new BsonString("{}")));
                    collectDiff(src.asDocument(), null, patch, pointer, true);
                } else {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("remove"))
                        .append("path", new BsonString(pointer)));
                    collectDiff(src.asDocument(), null, patch, pointer, false);
                }
                break;
            case ARRAY:
                if (tgt != null && tgt.isArray()) {
                    return collectDiff(src.asArray(), tgt.asArray(), patch, pointer, false);
                } else if (tgt != null) {
                    collectDiff(src.asArray(), null, patch, pointer, false);
                    collectDiff(tgt.asArray(), null, patch, pointer, true);
                } else if (add) {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("add"))
                        .append("path", new BsonString(pointer))
                        .append("value", new BsonString("[]")));
                    collectDiff(src.asArray(), null, patch, pointer, true);
                } else {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("remove"))
                        .append("path", new BsonString(pointer)));
                    collectDiff(src.asArray(), null, patch, pointer, false);
                }
                break;
            default:
                if (add) {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("add"))
                        .append("path", new BsonString(pointer))
                        .append("value", src));
                } else if (tgt == null) {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("remove"))
                        .append("path", new BsonString(pointer)));
                } else if (!src.equals(tgt)) {
                    patch.add(new BsonDocument()
                        .append("op", new BsonString("replace"))
                        .append("path", new BsonString(pointer))
                        .append("value", tgt));
                }
        }
        return patch;
    }
    
    private static BsonArray collectDiff(BsonDocument src, BsonDocument tgt, BsonArray patch, String pointer, boolean add) {
        for (Map.Entry<String, BsonValue> entry : src.entrySet()) {
            final String property = entry.getKey();
            final BsonValue tvalue = tgt == null ? null : tgt.get(property);
            if (tvalue != null && tvalue.getBsonType() == entry.getValue().getBsonType()) {
                collectDiff(entry.getValue(), tvalue, patch, pointer + "/" + property, add);
            } else {
                collectDiff(entry.getValue(), null, patch, pointer + "/" + property, add);
                if (tvalue != null) {
                    collectDiff(tvalue, entry.getValue(), patch, pointer + "/" + property, true);
                }
            }
        }
        if (tgt != null) {
            for (Map.Entry<String, BsonValue> entry : tgt.entrySet()) {
                final String property = entry.getKey();
                if (!src.containsKey(property)) {
                    collectDiff(entry.getValue(), null, patch, pointer + "/" + property, true);
                }
            }
        }
        return patch;
    }

    private static BsonArray collectDiff(BsonArray src, BsonArray tgt, BsonArray patch, String pointer, boolean add) {
        if (tgt == null || add) {
            for (int i = 0, n = src.size(); i < n; i++) {
                collectDiff(src.get(i), null, patch, pointer + "/" + i, add);
            }
        } else {
            final int size = Math.min(src.size(), tgt.size());
            for (int i = 0; i < size; i++) {
                collectDiff(src.get(i), tgt.get(i), patch, pointer + "/" + i, false);
            }
            for (int i = size, n = src.size(); i < n; i++) {
                collectDiff(src.get(i), null, patch, pointer + "/" + i, false);
            }
            for (int i = size, n = tgt.size(); i < n; i++) {
                collectDiff(tgt.get(i), null, patch, pointer + "/" + i, add);
            }
        }
        return patch;
    }
}
