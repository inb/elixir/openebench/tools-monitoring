/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReturnDocument;
import static es.bsc.inb.elixir.openebench.elixibilitas.mongo.ToolsMonitoringMeta.LICENSE_PROPERTY;
import static es.bsc.inb.elixir.openebench.elixibilitas.mongo.ToolsMonitoringMeta.LICENSE_PROPERTY_VALUE;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.security.Principal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.bson.BsonArray;
import org.bson.BsonDateTime;
import org.bson.BsonDocument;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.RawBsonDocument;
import org.bson.conversions.Bson;

/**
 * @author Dmitry Repchevsky
 */

public class ToolsRepository {
    
    public final static String COLLECTION = "tools";
    
    private final MongoDatabase database;
    private final RepositotyLogger logger;
    
    public ToolsRepository(MongoDatabase database) {
        this.database = database;
        
        final MongoCollection<BsonDocument> col = database
                .getCollection(COLLECTION + RepositotyLogger.LOG_SUFFIX, BsonDocument.class);

        logger = new RepositotyLogger(col);
    }
    
    public long count() {
        return database.getCollection(COLLECTION).countDocuments();
    }

    public BsonDocument get(String id) {
        try {
            final MongoCollection<BsonDocument> col = 
                    database.getCollection(COLLECTION, BsonDocument.class);
            
            final Bson filter = ToolsRepositoryQueries.findByIdQuery(id);
            final BsonDocument bson = col.find(filter).first();
            if (bson != null) {
                final BsonDocument pk = bson.getDocument("_id");
                bson.append("@type", pk.get("type", new BsonNull()));
                bson.append("namespace", pk.get("nmsp", new BsonNull()));
                bson.append("label", pk.get("id", new BsonString("")));
                bson.append("softwareVersion", pk.get("ver", new BsonNull()));
            }
            return bson;
        } catch(Exception ex) {
            Logger.getLogger(ToolsRepository.class.getName()).log(Level.SEVERE, "", ex);
        }
        return null;
    }

    /**
     * Writes all documents found in input stream.
     * The writes are postponed and happen when result stream is read.
     * 
     * @param in the stream that contains documents to be written
     * @param principal user's principal
     * 
     * @return the stream of written documents.
     */
    public Stream<BsonDocument> write(Stream<BsonDocument> in, Principal principal) {
        return in.map(d -> put(d, principal));
    }

    public BsonDocument put(BsonDocument bson, Principal principal) {

        BsonValue _id = bson.get("_id");
        if (_id == null) {
            return null; 
        }
        
        switch(_id.getBsonType()) {
            case DOCUMENT: _id = ToolsPrimaryKeys.getID(_id.asDocument()); break;
            case STRING: bson.append("_id", ToolsPrimaryKeys.getPrimaryKey(_id.asString())); break;
            default: return null;
        }
        
        try {
            final MongoCollection<BsonDocument> col = 
                    database.getCollection(COLLECTION, BsonDocument.class);

            final long timestamp = ZonedDateTime.now(ZoneId.of("Z")).toInstant().toEpochMilli();
            bson.append("dateModified", new BsonDateTime(timestamp));

            // remove metadata before saving to the database
            bson.remove("@id");
            bson.remove("@type");
            bson.remove("namespace");
            bson.remove("label");
            bson.remove("softwareVersion");
            bson.remove(LICENSE_PROPERTY);
            
            final FindOneAndReplaceOptions opt = new FindOneAndReplaceOptions().upsert(true).
                    projection(Projections.excludeId()).returnDocument(ReturnDocument.BEFORE);

            final BsonDocument pk = bson.getDocument("_id");
            final Bson filter = Filters.eq("_id", pk);
            final BsonDocument old = col.findOneAndReplace(filter, bson, opt);

            // replace the document '_id' with string format
            bson.append("_id", _id);
            
            logger.log(principal, old, bson);

            bson.append("@type", pk.get("type", new BsonNull()));
            bson.append("namespace", pk.get("nmsp", new BsonNull()));
            bson.append("label", pk.get("id", new BsonString("")));
            bson.append("softwareVersion", pk.get("ver", new BsonNull()));
            bson.append(LICENSE_PROPERTY, new BsonString(LICENSE_PROPERTY_VALUE));

            return bson;
        } catch(Exception ex) {
            Logger.getLogger(ToolsRepository.class.getName()).log(Level.SEVERE, "", ex);
        }
        
        return null;
    }
    
    /**
     * Write all tools as a raw stream of BSON Objects.
     * 
     * @param out the stream for writing the tools
     * @param skip number of tools to skip or null
     * @param limit number of tools to get or null
     */
    public void stream(OutputStream out, Long skip, Long limit) {
        try {
            final MongoCollection<RawBsonDocument> col = 
                    database.getCollection(COLLECTION, RawBsonDocument.class);
            
            FindIterable<RawBsonDocument> iter = col.find();
            if (skip != null && skip > 0) {
                iter = iter.skip(skip.intValue());
            }
            if (limit != null && limit > 0) {
                iter = iter.limit(limit.intValue());
            }

            try (WritableByteChannel channel = Channels.newChannel(out);
                 MongoCursor<RawBsonDocument> cursor = iter.iterator()) {
                while (cursor.hasNext()) {
                    final RawBsonDocument doc = cursor.next();
                    channel.write(doc.getByteBuffer().asNIO());
                }
            }
        } catch(IOException ex) {
            Logger.getLogger(ToolsRepository.class.getName()).log(Level.SEVERE, "", ex);
        }
    }

    public BsonArray findLog(String id, String jpointer, String from, String to, Integer limit) {
        return logger.findLog(id, jpointer, from, to, limit);
    }
}
