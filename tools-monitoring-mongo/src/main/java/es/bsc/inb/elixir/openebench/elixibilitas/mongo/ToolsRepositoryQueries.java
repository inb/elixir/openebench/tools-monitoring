/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.elixibilitas.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;

/**
 * @author Dmitry Repchevsky
 */

public class ToolsRepositoryQueries {

    public static Bson findByIdQuery(String id) {
        final String[] nodes = id.split("/");

        final String[] _id = nodes[0].split(":");
        if (_id.length > 2) {
            if (nodes.length > 1) {
                return Filters.and(Filters.eq("_id.id", _id[1]),
                        Filters.eq("_id.nmsp", _id[0]),
                        Filters.eq("_id.ver", _id[2]),
                        Filters.eq("_id.type", nodes[1]));
            }
            return Filters.and(Filters.eq("_id.nmsp", _id[0]), 
                    Filters.eq("_id.id", _id[1]), 
                    Filters.eq("_id.ver", _id[2]),
                    Filters.or(Filters.exists("_id.type", false), 
                               Filters.eq("_id.type", null)));
       } else if (_id.length > 1) {
            if (nodes.length > 1) {
                return Filters.and(Filters.eq("_id.id", _id[1]),
                        Filters.eq("_id.nmsp", _id[0]),
                        Filters.eq("_id.type", nodes[1]));
            }
            return Filters.and(Filters.eq("_id.id", _id[1]), Filters.eq("_id.nmsp", _id[0]));
        }

        return Filters.eq("_id", new BasicDBObject("id", _id[0]));
    }    
}
